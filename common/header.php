<!DOCTYPE html>
<html class="<?php echo get_theme_option('Style Sheet'); ?>" lang="<?php echo get_html_lang(); ?>">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php if ($description = option('description')): ?>
    <meta name="description" content="<?php echo $description; ?>">
    <?php endif; ?>

    <?php
    if (isset($title)) {
        $titleParts[] = strip_formatting($title);
    }
    $titleParts[] = option('site_title');
    ?>
    <title><?php echo implode(' &middot; ', $titleParts); ?></title>

    <?php echo auto_discovery_link_tags(); ?>

    <!-- Plugin Stuff -->
    <?php fire_plugin_hook('public_head', array('view'=>$this)); ?>

    <!-- Stylesheets -->
    <?php
    queue_css_url('//fonts.googleapis.com/css?family=Ubuntu:300,400,500,700,300italic,400italic,500italic,700italic');
    queue_css_file(array('iconfonts', 'normalize', 'style'), 'screen');
    queue_css_file('print', 'print');
    echo head_css();
    ?>

    <!-- JavaScripts -->
    <?php queue_js_file('vendor/modernizr'); ?>
    <?php queue_js_file('vendor/selectivizr'); ?>
    <?php queue_js_file('jquery-extra-selectors'); ?>
    <?php queue_js_file('vendor/respond'); ?>
    <?php queue_js_file('globals'); ?>
    <?php echo head_js(); ?>

<link rel="shortcut icon" href="http://www.consumoresponsavel.org.br/wp-content/uploads/2014/11/favicon.ico" />

<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-55ffd59237fda3fd" async="async"></script>

<!-- Start Open Web Analytics Tracker -->
<script type="text/javascript">
//<![CDATA[
var owa_baseUrl = 'http://owa.eita.org.br/';
var owa_cmds = owa_cmds || [];
owa_cmds.push(['setSiteId', '3b049f7409166a0e091b230d3688b5cc']);
owa_cmds.push(['trackPageView']);
owa_cmds.push(['trackClicks']);
owa_cmds.push(['trackDomStream']);

(function() {
	var _owa = document.createElement('script'); _owa.type = 'text/javascript'; _owa.async = true;
	owa_baseUrl = ('https:' == document.location.protocol ? window.owa_baseSecUrl || owa_baseUrl.replace(/http:/, 'https:') : owa_baseUrl );
	_owa.src = owa_baseUrl + 'modules/base/js/owa.tracker-combined-min.js';
	var _owa_s = document.getElementsByTagName('script')[0]; _owa_s.parentNode.insertBefore(_owa, _owa_s);
}());
//]]>
</script>
<!-- End Open Web Analytics Code -->

</head>
<?php echo body_tag(array('id' => @$bodyid, 'class' => @$bodyclass)); ?>
    <?php fire_plugin_hook('public_body', array('view'=>$this)); ?>

<section id="top">
      <div class="row">
	<div class="col-sm-12">
	    <ul id="topmenu" class="nav nav-pills">
		<li id="menu-item-47" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-47"><a href="http://www.consumoresponsavel.org.br/">Portal</a></li>
		<li id="menu-item-49" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-49"><a href="http://mapa.consumoresponsavel.org.br">Mapa</a></li>
		<li id="menu-item-48" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item  menu-item-home menu-item-48"><a href="http://noticias.consumoresponsavel.org.br/">Notícias</a></li>
<li id="menu-item-50" class="menu-item current_page_item menu-item-type-custom menu-item-object-custom menu-item-50"><a href="http://biblioteca.consumoresponsavel.org.br">Biblioteca</a></li>
		<li id="menu-item-81" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-81"><a href="http://www.consumoresponsavel.org.br/?page_id=2171">O que é</a></li>
		<li id="menu-item-85" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-85"><a href="http://www.consumoresponsavel.org.br/?page_id=2193">Quem somos</a></li>
	    </ul>
	</div>
	<div class="col-md-2">
	    <ul class="nav nav-pills navbar-right" id="socialmenu">
                <li class="feed-rss"><a href="http://noticias.consumoresponsavel.org.br/?feed=atom" title="Feed RSS"><i class="fa fa-lg fa-rss"></i></a></li>
		
		<li class="facebook"><a href="https://www.facebook.com/aplicativoresponsa/?fref=ts" title="Siga-nos no Facebook" target="_blank"><i class="fa fa-lg fa-facebook"></i></a></li>
                                                    
               <li class="instagram"><a href="https://www.instagram.com/_responsa/" title="Siga-nos no instagram" target="_blank"><i class="fa fa-lg fa-instagram"></i></a></li>
                                                    
               <li class="googleplus"><a href="https://soundcloud.com/user-589244400" title="Ouça-nos no SoundCloud!" target="_blank"><i class="fa fa-lg fa-soundcloud"></i></a></li>
                                                  


	    </ul>
	</div>
</div>
</section>
    <div id="wrap">
        <header>
            <a href="http://biblioteca.consumoresponsavel.org.br">
            <div id="site-title">
    <h1 class="title">Biblioteca<?php //echo implode(' &middot; ', $titleParts); ?></h1>
            </div>
	    </a>
            <div id="search-container">
                <?php if (get_theme_option('use_advanced_search') === null || get_theme_option('use_advanced_search')): ?>
                <?php echo search_form(array('show_advanced' => true)); ?>
                <?php else: ?>
                <?Php echo search_form(); ?>
                <?php endif; ?>
            </div>
            <?php fire_plugin_hook('public_header', array('view'=>$this)); ?>
        </header>

        <nav class="top">
            <?php echo public_nav_main(); ?>
        </nav>

        <div id="content">
            <?php
                if(! is_current_url(WEB_ROOT)) {
                  fire_plugin_hook('public_content_top', array('view'=>$this));
                }
            ?>
