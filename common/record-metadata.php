<?php if (isset($elementsForDisplay['Vídeo Item Type Metadata'])): ?>
	<?php $elementsForDisplay = array('Vídeo Item Type Metadata' => $elementsForDisplay['Vídeo Item Type Metadata']) + $elementsForDisplay; ?>
<?php endif; ?>
<?php foreach ($elementsForDisplay as $setName => $setElements): ?>
	<?php if (count($setElements)): ?>
		<div class="element-set">
		  <?php if ($showElementSetHeadings && !in_array($setName, array('Dublin Core', 'Vídeo Item Type Metadata', 'Collection Items'))): ?>
		  	<h2><?php echo html_escape(__($setName)); ?></h2>
		  <?php endif; ?>
		  <?php foreach ($setElements as $elementName => $elementInfo): ?>
				<div id="<?php echo text_to_id(html_escape("$setName $elementName")); ?>" class="element">
					<?php if (!in_array($elementName, array('Player'))): ?>
			    	<h3><?php echo html_escape(__($elementName)); ?></h3>
			    <?php endif; ?>
			    <?php foreach ($elementInfo['texts'] as $text): ?>
			        <div class="element-text"><?php echo $text; ?></div>
			    <?php endforeach; ?>
				</div><!-- end element -->
		  <?php endforeach; ?>
		</div><!-- end element-set -->
	<?php endif; ?>
<?php endforeach;
